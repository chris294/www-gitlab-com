(function() {
  var switchFormsBool = false;

  var switchButtons = document.getElementsByClassName('switch-forms');
  var securityNoticesForm = document.getElementById('security-notices');
  var newsletterAnchor = window.location.hash.substr(1);

  securityNoticesForm.style.display = 'none';

  function switchForms() {
    switchFormsBool = !switchFormsBool;
    if (switchFormsBool) {
      securityNoticesForm.style.display = 'flex';
    } else {
      securityNoticesForm.style.display = 'none';
    }
  }

  if (newsletterAnchor === 'security-notices') {
    switchForms();
  }

  for (var i = 0; i < switchButtons.length; i++) {
    switchButtons[i].addEventListener('click', switchForms);
  }
})();
